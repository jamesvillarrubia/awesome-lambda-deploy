npm install -g json
cat claudia.json | json api
#CI_PERSONAL_TOKEN=$1
#CI_PROJECT_ID=$2
echo $LAMBDA_ID
LAMBDA_ID=$(cat claudia.json | json api.id)
LAMBDA_ZONE=$(cat claudia.json | json lambda.region)
echo "ID: $LAMBDA_ID"
echo "ZONE: $LAMBDA_ZONE"
RESPONSE=$(curl -s -o /dev/null -w %{http_code} -H "Content-Type: application/json" -X POST -d '{"username":"xyz","password":"xyz"}' "https://3ey0nvq4y4.execute-api.us-east-1.amazonaws.com/production")
echo $RESPONSE

if [[ $RESPONSE == "200" ]]; then
    echo "Tagging this branch as stable"
    DATE_TAG=$(date +%s)
    STABLE_TAG="stable--$DATE_TAG"
    echo "STABLE @ $STABLE_TAG"
    CURRENT_TAG=$(git rev-parse HEAD)
    echo "$CURRENT_TAG"
    curl -s -X --verbose --request POST --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/tags?tag_name=${STABLE_TAG}&ref=${CURRENT_TAG}"
    echo "\n"
else 
    LAST_STABLE_TAG=$(git describe --tags --match "stable--*" --abbrev=0)
    echo "LAST STABLE @ $LAST_STABLE_TAG"
    TRIGGER=$(curl -s -X --verbose --request POST --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" --form description="Temporary Trigger" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/triggers")
    TRIGGER_TOKEN=$(echo ${TRIGGER} | json token)
    TRIGGER_ID=$(echo ${TRIGGER} | json id)
    echo $TRIGGER_TOKEN
    curl -X --verbose --request POST --form token=${TRIGGER_TOKEN} --form ref=${LAST_STABLE_TAG} "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/trigger/pipeline"
    curl -X --verbose --request DELETE --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/triggers/${TRIGGER_ID}"

    exit 1
fi  