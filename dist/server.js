'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

require('babel-core/register');
require('babel-polyfill');

var _require = require('./index.js'),
    service = _require.service;

var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var server = express();
server.use(cors());
server.use(bodyParser.json()); // for parsing application/json
server.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

/**
 * This function can be run as a local HTTP microservice.  This is helpful for testing.
 *
 * @param {any} req The request object
 * @param {any} res The response object
 */
server.all('/', function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(req, res, next) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.t0 = res;
            _context.next = 4;
            return service(req, res);

          case 4:
            _context.t1 = _context.sent;

            _context.t0.send.call(_context.t0, _context.t1);

            if (!res.finished) {
              res.send('Service responded with null.');
            }
            _context.next = 13;
            break;

          case 9:
            _context.prev = 9;
            _context.t2 = _context['catch'](0);

            res.status(400);
            throw new Error(process.env.AWS_FUNCTION_NAME + ' has thrown an error: ' + _context.t2);

          case 13:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 9]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());

//module.exports = server

server.listen(process.env.SER_PORT);
console.log(process.env.AWS_FUNCTION_NAME + ' is running on ' + process.env.SER_PORT);