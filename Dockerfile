FROM lambci/lambda:build-nodejs6.10

RUN cd /

COPY . .

RUN npm install claudia -g
RUN npm install mocha -g 
RUN npm install semver -g
RUN npm install -g env-cmd
RUN npm install
RUN npm run build
ENTRYPOINT npm run c-update && npm test

