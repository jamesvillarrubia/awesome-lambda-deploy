

[![build status](https://gitlab.com/jamesvillarrubia/awesome-lambda-deploy/badges/master/build.svg)](https://gitlab.com/jamesvillarrubia/awesome-lambda-deploy/commits/master)

# Getting Started

Make sure you have the following installed:

- npm
- node
- nvm

For good practice you should be using node 6.10.3 as that's what AWS lambda is using.  Keeps your environment and their environment nice and tidy.

```bash
nvm install 6.10.3
nvm use 6.10.3
```

Run the following command to install baseline libraries

```bash
npm install -g mocha nodemon env-cmd babel-cli semver claudia
```

Then run

```bash
npm install
```

Add your module name to ```package.json``` -> config -> module_name (this is what your function will be called).  Make sure you have your AWS credentials in ~/.aws/credentials or create a new profile.  Add the profile name to the config.  We're going to use "aws-test" as our profile.

```json
 "config": {
    "aws_profile": "aws-test",
    "module_name": "awesome-module"
  },
```

In ~/.aws/credentials, you should have:

```text
[aws-test]
aws_access_key_id = XXXXXXXXXXXXXXX
aws_secret_access_key = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

Next, copy /config/config.example.json into config.production.json, config.staging.json, and config.local.json, all kept inside the config folder.  Change the parameter AWS_FUNCTION_NAME to whatever you named your module.  In this example, it's "awesome-module."



# How the deployment works

1. The gitlab-ci runner will be used to spin up a container in gitlab's cloud (free) and compile the code.  Because some npm packages require native compilation, we need to compile them in the *exact* environment that the Lambda function will run in.  This was the reason this whole process was created.
2. The image our runner (.gitlab-ci.yml) uses is the [lambCI](https://github.com/lambci/lambci) version of the Amazon Linux AMI with node 6.10.3 installed which is what we need.
3. Our runner will then load in the aws credentials (from the secret variables in gitlab) to the temporary lambCI container.
4. That AWS profile will be used to compile the lambda function inside the temporart AWS environment. See the dockerfile ```RUN npm``` and ```RUN npm install claudia -g```
5. The entrypoint on the container will be ```npm run c-update``` which is the update script to the lambda function.  This assumes that the claudia.json file has *already* been created.  Typically this claudia.json is created when you first do local testing.  See below...

Because deployment is a little complex, there are two ways of testing your function locally before deploying.  One is to just spin up the local server instance, like a normal express server and hit it with Postman, etc.  The other is to spin up a docker image with a proxy that does the same thing, but you know it was compiled via the lambda environment.

## Validate the Service works

1. Before adding your code, let's take a stab at running the empty script as a service on port 6010 and making sure everything works.
  ```bash
  # This starts your service locally
  npm start
  # In a new terminal tab, tun this curl script sends a simple blob to the service
  curl -H "Content-Type: application/json" -X POST -d '{"username":"xyz","password":"xyz"}' http://localhost:6010/
  ```
1. You should get back ```{"username":"xyz","password":"xyz"} ```.

Congrats, you've validated the internal service.  Onto the next layer...


## Use the local server (and run the mocha test.js)

```bash
# run the test once after building
npm run test
# run the test repeatedly and rebuild on change via nodemon
npm run test-loop
```

## Testing the setup/deployment process locally in docker

1. Make sure you have docker installed with the CLI.
1. Run  ```nmp run dockerize```.  This will create a local docker container and test/create/update the lambda deployment function.
1. You should see the following response:

  ```bash
    awesome-module
      ✓ This is a sample test
  ```


# Setting up deployment

1. First, you must create a gitlab repository for your lambda script.  Starting with a fork of this repo is a good way to do it.
2. Make new IAM credentials for this lambda script (or lambda in general).  I typically create a "lambda-deploy" user with the following permissions:
    - AWSLambdaFullAccess
    - AmazonAPIGatewayAdministrator
    - IAMFullAccess
3. Next, add the AWS credentials to the lambda-deploy user to the project as variables.  In gitlab this is under settings->pipeline->"Secret Variables".  They should be stored under the same names as the .gitlab-ci.yml file: GL\_AWS\_ACCESS\_KEY\_ID and GL\_AWS\_SECRET\_ACCESS\_KEY respectively.
4. Finally, there's a semantic versioning script in this boilerplate as well. To keep your pipelines from going nuts and deploying on every commit or tag, we're going to put a gitlab personal key in the repo as well.  This allows the script to call the gitlab api and cancel (not fail) a commit that should not deploy.  Go to your account in gitlab -> Settings -> Access Tokens and then give it API access and a name and copy the resulting token.
5. Go to the repo and add the token as a "Secret Variable" with the name 'CI\_PERSONAL\_TOKEN'

# Versioning your script

Versioning the lambda script can be a great way of keeping multiple versions of your lambda script available under different tags and always accessible to different environments.  With this script, we generate 3 different lambda versions.

```bash
# the real link will be in the results of your deployment call.
https://XXXXXXXX.execute-api.us-east-1.amazonaws.com/local
https://XXXXXXXX.execute-api.us-east-1.amazonaws.com/staging
https://XXXXXXXX.execute-api.us-east-1.amazonaws.com/production

# production -> master remote
# staging -> develop remote
# local -> develop local

```

We also have created a way to trigger only the important builds and tag them with semver for later use.  This logic is in ```/config/semver.sh```.

### TRIGGERING A BUILD

Not every commit needs a build.  And semantic versioning is a valuable way of documenting and controlling releases.  This repo will do this for you. To trigger a build, you must use one of the following as your commit message:

 - MAJOR UPDATE: use "semver:release" or "semver:major" as your commit message.
 - MINOR UPDATE: use "semver:feature" or "semver:minor" as your commit message.
 - PATCH UPDATE: use "semver:hotfix" or "semver:fix" or "semver:patch" as your commit message.



# Add Your Code

Add your code into the service function of ./scr/index.js
Add necessary modules to package.json
Alter the test.js as necessary


